import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost user=kamil")

inventory_limit = 100

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
buy_increase_inventory = "UPDATE Inventory SET amount = amount + %(amount)s where USERNAME = %(username)s AND product = %(product)s"
amount_in_inventory_request = "SELECT SUM(amount) FROM Inventory WHERE username = %(username)s"

def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            try: 
                cur.execute(amount_in_inventory_request, obj)
                if (initial_amount := cur.fetchone()[0]) and (initial_amount + amount > inventory_limit):
                     raise Exception("Your inventory is full")
                cur.execute(buy_increase_inventory, obj)
            except psycopg2.errors.CheckViolation as e:
                raise Exception(f"It is not possible to add {product} to {username}'s inventory")

            conn.commit()

if __name__ == "__main__":
    buy_product("Bob", "marshmello", 99)